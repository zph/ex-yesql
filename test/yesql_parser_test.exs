defmodule Yesql.ParserTest do
  use ExUnit.Case
  doctest Yesql.Parser

  @sql_one """
  -- name: fn-name
  -- This is our description.
  select * from table where thing
  is LIKE :foo;

  -- name: fn-two
  -- This is our description.
  select * from ?;
  """ |> to_char_list

  @r Yesql.Parser.parse(@sql_one)

  test "that fn names are formatted in snake case" do
    assert List.first(@r).name == "fn_name"
  end

  test "that both sql lines are gathered" do
    assert List.first(@r).sql == "select * from table where thing\nis LIKE :foo"
  end

  test "that we capture both results" do
    assert length(@r) == 2
  end
end

defmodule Yesql.Parser.StatementTest do
  use ExUnit.Case
  doctest Yesql.Parser.Statement

  test "that named params are captured" do
    sql = "select * from table where age IS :foo and gender\n is ?;"
    r = Yesql.Parser.Statement.parse(sql)

    assert r == %{named: [":foo"], placeholder: ["?"]}
  end

  test "that all named params are captured" do
    r = Yesql.Parser.Statement.parse("select * from table where age IS :foo and gender is ? and int is :int;")

    assert r == %{named: [":foo", ":int"], placeholder: ["?"]}
  end

  test "that queries without placeholders or named params are parsable" do
    r = Yesql.Parser.Statement.parse("select * from table;")
    assert r == %{named: [], placeholder: []}
  end
end
