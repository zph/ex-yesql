Definitions.

WS       = ([\s\t])
NL       = (\r?[\n])
STR      = ([^\\\s\t\?;]+)
PLACEHOLDER_PARAM = ([\?])
SC       = ([;])
NAMED_PARAM = (:[A-Za-z0-9\-_]+)

Rules.

{PLACEHOLDER_PARAM} : {token, {placeholder_param, TokenLine, TokenChars}}.
{NAMED_PARAM} : {token, {named_param, TokenLine, TokenChars}}.

{STR}                                      : skip_token.
{NL}                                       : skip_token.
{WS}+                                      : skip_token.
{SC} : skip_token.

Erlang code.

-define(debug(T), begin io:format(user, "Token ~p~n", [T]), T end).
