Nonterminals params param named placeholder.
Terminals named_param placeholder_param sc.
Rootsymbol params.

params -> param : ['$1'].
params -> param params : ['$1'|'$2'].

param -> named : {named, '$1'}.
param -> placeholder : {placeholder, '$1'}.

named -> named_param : unwrap('$1').
placeholder -> placeholder_param : unwrap('$1').

Erlang code.

unwrap({_,_,V}) -> V.
