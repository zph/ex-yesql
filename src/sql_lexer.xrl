Definitions.

WS       = ([\s\t])
NL       = (\r?[\n])
HYPHENS  = (--)
STR      = (.*)
NAME_TAG = name:
NAME     = ([A-Za-z0-9\-_]+)
BREAK    = (\n\n)

Rules.

{NL}                                       : skip_token.
{HYPHENS}                                  : {token, {hypens, TokenLine, TokenChars}}.
{HYPHENS}{WS}?{NAME_TAG}{WS}?({NAME}){WS}? : {token, {name, TokenLine, TokenChars}}.
{HYPHENS}{WS}?({STR})+                     : {token, {comment, TokenLine, TokenChars}}.
{STR}                                      : {token, {sql, TokenLine, TokenChars}}.
{BREAK}                                    : {token, {break, TokenLine, TokenChars}}.

{WS}+                                      : skip_token.

Erlang code.

-define(debug(T), begin io:format(user, "Token ~p~n", [T]), T end).
