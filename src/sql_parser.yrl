Nonterminals queries query sqls tsql tname tcomment.
Terminals name comment sql break.
Rootsymbol queries.

queries -> query : ['$1'].
queries -> query queries : ['$1'|'$2'].

query -> tname tcomment sqls break: {query, '$1', '$2', '$3'}.
query -> tname tcomment sqls: {query, '$1', '$2', '$3'}.

sqls -> tsql : '$1'.
sqls -> tsql sqls : '$1' ++ "\n" ++ '$2'.

tname -> name : unwrap('$1').
tcomment -> comment : unwrap('$1').
tsql -> sql : unwrap('$1').

Erlang code.

unwrap({_,_,V}) -> V.
