defmodule Yesql.Parser.Command do
  defstruct name: "", description: "", sql: "", params: []
end

defmodule Yesql.Parser.Statement do
  def recompile_xrl() do
    :leex.file('src/statement_lexer.xrl')
  end

  def parser([]), do: []
  def parser(tokens), do: :statement_parser.parse(tokens)

  def parse(sql) do
    r = with {:ok, tokens, _} <- :statement_lexer.string(sql |> to_char_list),
         {:ok, response} <- parser(tokens),
         do: response

    place = for {:placeholder,v} <- r, do: to_string(v)
    named = for {:named,v} <- r, do: to_string(v)
    %{named: named, placeholder: place}
  end
end

defmodule Yesql.Parser do
  def recompile_xrl() do
    :leex.file('src/sql_lexer.xrl')
  end

  def parse(sql) do
    with {:ok, tokens, _} <- :sql_lexer.string(sql |> to_char_list),
         {:ok, response} <- :sql_parser.parse(tokens),
    do: into_queries(response)
  end

  def into_queries(parsed) do
    Enum.map(parsed, fn({:query, n,d,s}) ->
      sql = sanitize_sql(s)
      params = Yesql.Parser.Statement.parse(sql)
      %Yesql.Parser.Command{name: strip_line_leaders(n) |> to_underscore,
                            description: strip_line_leaders(d),
                            sql: sql,
                            params: params}
    end)
  end

  def strip_line_leaders(s) do
    s
    |> to_string
    |> String.replace(~r"^-- ?( ?name:)? ?", "")
  end

  def to_underscore(s) do
    s |> String.replace(~r"[- ]", "_")
  end

  def sanitize_sql(s) do
    s
    |> to_string
    |> String.replace(~r";$", "")
  end
end
